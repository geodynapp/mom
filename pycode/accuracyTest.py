#!/usr/bin/env python

from __future__ import print_function, division
from matplotlib import pyplot as plt
import pandas as pd
import h5py as h5
import sys
import numpy as np

if len(sys.argv) != 2:
    print("Usage: {} file.h5".format(sys.argv[0]))
    quit()


# Put averything in memory... around 5 Gb
h5file = h5.File(sys.argv[1])
df = pd.DataFrame(h5file['FP'][:])
df.index = df.pop('Id')
h5file.close()

# The fitness
r = np.array([ np.float64(c[3:]) for c in df.columns if c[:3] == 'FP_'])


#
# Absolute error in isothermal graphs
#
def fpKn(x, r=None, relative = False):
    exp = int(x.pop('order'))
    xp = (r**(exp-1)/sum(r**k for k in range(exp)))
    if relative:
        return (x/xp-1).abs()
    else:
        return (x - xp).abs()

cols = [ c for c in df.columns if c[:3] == 'FP_' ]
cols.append('order')
IT = df[df.degree_max == df.degree_min][cols]
print('Maximal absolute error for the Isothermal graphs (const. degree):', IT.apply(fpKn, axis=1, r=r).max().max())
print('Maximal relative error for the Isothermal graphs (const. degree):', IT.apply(fpKn, axis=1, r=r, relative=True).max().max())

#
# Absolute error for fitness r=1
#
print('Maximal absolute error for r = 1:', np.abs(df['FP_1.0'] - 1/df['order']).max() )
print('Maximal relative error for r = 1:', np.abs(df['FP_1.0'] * df['order'] - 1).max() )

#
#Absolute error for Bipartite graphs (including stars)
#
def fpBP(x, r=None, relative=False):
    c = x.pop('degree_max')
    v = x.pop('degree_min')
    n = c + v
    
    hc = (r*v+c)/((v+c*r)*r)
    hv = (v+c*r)/((v*r+c)*r)
    
    xp = ((c*(hc-1) + v*(hv-1))/(n*(hc**c*hv**v-1)))
    
    if relative:
        return (x/xp-1).abs()
    else:
        return (x - xp).abs()

cols = [ c for c in df.columns if c[:3] == 'FP_' ]
cols.extend(['degree_min', 'degree_max'])
BP = df[(df.degree_max + df.degree_min == df.order) & df.is_bipartite & (df.degree_min>1)][cols]
print('Maximal absolute error for Complete Bipartite graphs (excluding Stars):', BP.apply(fpBP, axis=1, r=r).max().max())
print('Maximal relative error for Complete Bipartite graphs (excluding Stars):', BP.apply(fpBP, axis=1, r=r,relative=True).max().max())

BP = df[(df.degree_max + df.degree_min == df.order) & df.is_bipartite & (df.degree_min == 1)][cols]
print('Maximal absolute error for Star graphs:', BP.apply(fpBP, axis=1, r=r).max().max())
print('Maximal relative error for Star graphs:', BP.apply(fpBP, axis=1, r=r,relative=True).max().max())
