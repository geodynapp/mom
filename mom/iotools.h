
#ifndef _IOTOOLS_H
#define _IOTOOLS_H

/*
 * Read data from a file formated as explained in FileFormats.md:
 *
 * [ Id | N | size | edges ]
 *
 * `Id` (`uint32_t`) is the unique Id of the graph. The order comes from Sage.
 * `N` (`uint8_t`) is the number of nodes in the graph.
 * `size` (`uint_8`) is the number of edges.
 * `edges` is a stream of size bytes, each of them considered as a couple 
 * of 4bits integer. This structure is defined as a bitfield edge_t of above
 */
int ReadData(FILE* infile, uint32_t *Id, int *N, int *G, int *Deg );

/*
 * Write data to a file formated as explained in FileFormats.md:
 * Again, the format tries to be as simple and compact as possible. It contains 
 * matrices Q and b needed to compute the fixation probabilities as Qx=b.
 * These matrices are stored in a sparse format:
 *
 * [ Id | N | num_Q | Q | num_b | b ]
 *
 * where
 *  - Id (uint32_t) is the unique Id of the graph. 
 *  - N (uint8_t) is the number of nodes in the graph.
 *  - num_Q (uint_16) is the number nontrivial transitions.
 *  - Q this is a concatenation num_Q groups of 
 *
 *        [ A | B | Rn | Rd | In | Id ]
 *
 *    where A and B are the states (represented by uint16_t) and Rn, Rd, In and 
 *    Id are int16_t to form the corresponding value Rn/Rd*r+In/Ir.
 *  - num_b (uint_16) is the number of nontrivial entrances in b.
 *  - b is a concatenation similar to Q
 *       [ A | Rn | Rb | In | Id ]
 *    where A is a state (represented by a uint16_t) and Rn, Rd, In and Id 
 *    are int16_t to form the corresponding value Rn/Rd*r+In/Ir.

 */
void WriteData(FILE* outfile, uint32_t IdG, uint8_t N, int64_t* Rd, int64_t* Rn, int64_t* Id, int64_t* In);

#endif




