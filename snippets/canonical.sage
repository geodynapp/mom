import numpy as np
import h5py as h5

def graphID(Id):
    """Prints the edge list associated to the number Id. It is trivial to adapt it to produce a NetworkX or iGraph object for example
    """
    
    G = Graph()
    
    mask = np.uint64(1)
    u = 0
    v = 1
    while u < 10:
        if (np.uint64(Id) & mask):
            G.add_edge(u,v)
        
        mask = (mask << np.uint(1))
        v += 1
        
        if v > 10:
            u += 1
            v = u+1
    return G

def computeID(G):
    newID = np.uint64(0)
    for e in G.edges():
        if e[0] > e[1]:
            a = e[1] 
            d = e[0]-e[1]
        else:
            a = e[0] 
            d = e[1]-e[0]

        bit = 54 - (11 - a) * (10 - a) // 2 + d
        newID  = np.bitwise_or(newID, np.uint64(1 << bit) )
    
    return newID
    

infile =h5.File('fp_le10.hdf5', 'r+')
data = infile['FP'][:]

for i,oldID in enumerate(data['Id']):
    newID = computeID(graphID(oldID).canonical_label(algorithm = "bliss"))
    print '{:>10}'.format(i), oldID,' -> ', newID
    data['Id'][i] = newID
    
infile['FP'][:] = data

infile.close()


