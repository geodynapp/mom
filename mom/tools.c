#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "types.h"
#include "tools.h"
#include "iotools.h"


/*
 * Computes the greatest common divisor of 2 integers
*/
inline int64_t GCD(int64_t a, int64_t b) {
    int64_t tmp;
    while (b != 0) {
        tmp = a % b;
        a = b;
        b = tmp;
    }
    return (a>0)?a:-a;
}

/*
 * Checks if the polinomial a/b r + c/d is null
*/
inline int IsNull(int64_t a, int64_t b, int64_t c, int64_t d) {
    if( (a == 0) && (c == 0))
        return 1;
    return 0;
}

void MulTo(int64_t* rn, int64_t* rd, int64_t a, int64_t b, int64_t c, int64_t d) {
    //printf("Product %lli/%lli * %lli/%lli ",a,b,c,d);
    
    // To-Do: Add overflow comprobations
    if( (log2((double)a) + log2((double)c) > 63) || (log2((double)b) + log2((double)d) > 63)){
        printf(" ¡Overflow in a product! ");
        abort();
    }
        
    *rn = a * c;
    *rd = b * d;
    // Some simplifications
    if( *rn == 0 ){
        *rd = 1;
    } else {
        int64_t gcd = GCD(*rn, *rd);
        *rn = (*rn)/gcd;
        *rd = (*rd)/gcd;
    }
    //printf(" = %lli/%lli\n", *rn, *rd);
}

#define MAX(v,w) (((v)>(w))?(v):(w))

void AddTo(int64_t* rn, int64_t* rd, int64_t a, int64_t b, int64_t c, int64_t d) {
    
    // To-Do: Add overflow comprobations
    if( (MAX( log2((double)a) + log2((double)d) , log2((double)c) + log2((double)b) )+1>63)
            || (log2((double)b) + log2((double)d) > 63)){
        printf(" ¡Overflow in an addition! ");
        abort();
    }
    
    *rn = a * d + c * b;
    *rd = b * d;
    // Some simplifications
    if( *rn == 0 ){
        *rd = 1;
    } else {
        int64_t gcd = GCD(*rn, *rd);
        *rn = (*rn)/gcd;
        *rd = (*rd)/gcd;
    }
}

void PrintFraction(int64_t c, int64_t d){
    if(c==0){
        printf("0");
    } else {
        printf("%lli",c);
        if(d!=1) printf("/%lli",d);
    }
}



