# File Formats used by the `C` implementation

Here we briefly describe the format used by the Markov matrices calculator **implemented in `C`**. These formats are platform specific, that is, they depend on the endianness of the CPU. This is specially true in the case of the bitfield used for describe the edges. We use x86 so expect little endian in our files.

#### Binary edgelist format
The format is quite simple, a concatenation of registers as this:

```
[ Id | N | size | edges ]
```

where

* `Id` (`uint32_t`) is the unique Id of the graph. The order comes from Sage.
* `N` (`uint8_t`) is the number of nodes in the graph.
* `size` (`uint_8`) is the number of edges.
* `edges` is a stream of size bytes, each of them considered as a couple of 4bits integer. This structure is defined as a bitfield:
```C
typedef struct {
    uint8_t u : 4;
    uint8_t v : 4;
} edge_t;
```






