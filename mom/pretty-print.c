#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "tools.h"





/*
 * Prints the polinomial a/b r + c/d nicely
*/
inline void _PrettyPrint(int64_t a, int64_t b, int64_t c, int64_t d) {
    //It assumes that the number is not null
    int rused = 0;
    
    if(a!=0){
        rused = 1;
        switch(a){
            case -1:
                printf("-r");
                break;
            case 1:
                printf("r");
                break;
            default:
            printf("%lli*r", a);
        }
        if(b!=1)
            printf("/%lli", b);
    }
    if(c!=0){
        if(c<0){
            printf("%lli",c);
        } else{
            if(rused)
                printf("+");
            printf("%lli",c);
        }
        if(d!=1) printf("/%lli",d);
    }
}


/*inline void PrettyPrint(uint32_t IdG, int N, int64_t *Rn, int64_t *Rd, int64_t *In, int64_t *Id) {
    // Now, read the file and pretty print the matrices
    int i,j,x,printcomma;
    printf("%u\t%i\t{", IdG, N);
    printcomma=0;
    for( i = 1 ; i < (1<<N)-1 ; i++) {
        for(j = 0 ; j < N ; j++ ){
            x = (N+2) * i + j;
            if(!IsNull(Rn[x], Rd[x], In[x], Id[x])){
                if(printcomma){
                    printf(",");
                    printcomma=0;
                }
                printf( "(%i,%i):",i, i^(1<<j) );
                _PrettyPrint(Rn[x], Rd[x], In[x], Id[x]);
                printcomma=1;
            }
        }
        x = (N+2) * i + N;
        if(!IsNull(Rn[x], Rd[x], In[x], Id[x])){
            if(printcomma){
                printf(",");
                printcomma=0;
            }
            printf( "(%i,%i):",i,i);
            _PrettyPrint(Rn[x], Rd[x], In[x], Id[x]);
            printcomma=1;
        }
    }
    printf("}\t{");
    printcomma=0;
    for( i = 1 ; i < (1<<N)-1 ; i++) {
        x = (N+2) * i + N + 1;
        if(!IsNull(Rn[x], Rd[x], In[x], Id[x])){
            if(printcomma){
                printf(",");
                printcomma=0;
            }
            printf( "%i:",i);
            _PrettyPrint(Rn[x], Rd[x], In[x], Id[x]);
            printcomma = 1;
        }
    }
    printf("}\n");
}*/


int ReadFileAndPrint(FILE* infile){
    int i,j,x,printcomma;
    uint32_t IdG;
    Q_t Q;
    b_t b;
    uint8_t N;
    uint16_t num;
    
    //Id
    if(fread(&IdG, sizeof(uint32_t), 1, infile) == 0){
        return 0;
    }
    //graph order
    if(fread(&N, sizeof(uint8_t), 1, infile) == 0){
        printf("Error reading file\n");
        abort();
    }
    //Print the heading
    printf("%u\t%u\t{", IdG, N);
    printcomma=0;
    
    
    // Now Q
    if(fread(&num, sizeof(uint16_t), 1, infile) == 0){
        printf("Error reading file\n");
        abort();
    }
    
    for( ; num > 0 ; num-- ){
        if(fread(&Q, sizeof(Q_t), 1, infile) == 0){
            printf("Error reading file\n");
            abort();
        }
        
        if(printcomma){
            printf(",");
            printcomma=0;
        }
        printf( "(%i,%i):", Q.A, Q.B );
        _PrettyPrint( Q.Rn, Q.Rd, Q.In, Q.Id);
        printcomma=1;
    }
    printf("}\t{");
    
    //Finally b
    if(fread(&num, sizeof(uint16_t), 1, infile) == 0){
        printf("Error reading file\n");
        abort();
    }
    
    printcomma=0;
    for( ; num > 0 ; num-- ){
        if(fread(&b, sizeof(b_t), 1, infile) == 0){
            printf("Error reading file\n");
            abort();
        }
        if(printcomma){
            printf(",");
            printcomma=0;
        }
        printf( "%i:", b.A);
        _PrettyPrint( (int64_t)b.Rn, (int64_t)b.Rd, (int64_t)b.In, (int64_t)b.Id);
        printcomma=1;
    }
    
    printf("}\n");
    
    return 1;
}




/*
 * Entry point
*/
int main(int argc, char *argv[]) {
    // Counters
    int i;
    int j;
    int x;                  // Convoluted position in a 2d-matrix
    char *infilename;       // Input file name
    FILE *infile;           // Input file pointer
    
    setbuf(stdout, NULL);
    
    // Parse arguments
    if (argc < 2) {
        printf ("Usage:\n\t%s infile\n\n", argv[0]);
        return 1;
    }
    
    infilename = argv[1];
    
    infile = fopen(infilename, "rb");
    if(!infile){
        printf("Error! Unable to open '%s' for reading\n\n", infilename);
        return 1;
    }
    
    // OK we're ready for rock!
    while( ReadFileAndPrint(infile) );

    
    
    // Close files
    fclose(infile);
    // Free memory
}





/* 
*/