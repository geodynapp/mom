import h5py
import numpy as np

Graphs = {}
with gzip.open('EdgeLists.txt.gz','rt') as f:
    for line in f:
        idg,el = line.split('\t')
        idg = int(idg)
        el = eval(el)
        Graphs[idg] = nx.from_edgelist(el)
        if Graphs[idg].order() > 9:
            del Graphs[idg]
            break

PF = {}
with open('PF_LE9.csv') as f:
    for line in f:
        idg, prob = line.split('\t', maxsplit=1)
        PF[int(idg)] = np.fromstring(prob,sep='\t')


fich = h5py.File('fp_le9.hdf5', mode='rw')

data = np.empty((len(PF),len(PF[1])+2))
data[:] = np.nan

for i,idg in enumerate(PF):
    data[i,0] = idg
    data[i,1:41] = PF[idg]
    data[i,41] = Graphs[idg].order()

ds=fich.create_dataset('FP',data=data,dtype=np.float,fletcher32=True,compression='gzip',compression_opts=9,maxshape=(12000000,100),chunks=True)

col_names = [np.string_('Id')]
col_names.extend( [np.string_('FP_{}'.format(x)) for x in np.linspace(0.25,10,40)] )
col_names.append( np.string_('order') )

col_types = [np.string_('int')]
col_types.extend([np.string_('double')]*40)
col_types.append(np.string_('int'))

ds.attrs['col_names'] = col_names
ds.attrs['col_types'] = col_types

fich.close()