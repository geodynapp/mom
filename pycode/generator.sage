import gzip
import sys

k = 0
with gzip.open('/home/alvaro/Dropbox/Graphs9/EdgeLists.txt.gz', mode='a', buffering=1000) as fich:
    for order in range(2,11):
        for G in graphs(order):
            if G.is_connected():
                k+=1
                fich.write(str(k))
                fich.write('\t')
                fich.write(str([e[:2] for e in G.edges()]))
                fich.write('\n')
                
                if k%1000 == 0:
                    print k
                    sys.stdout.flush()
                
                
    