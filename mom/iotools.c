#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "tools.h"
#include "iotools.h"
#include "types.h"

/*
 * Read data from a file formated as explained in FileFormats.md:
 *
 * [ Id | N | size | edges ]
 *
 * `Id` (`uint32_t`) is the unique Id of the graph. The order comes from Sage.
 * `N` (`uint8_t`) is the number of nodes in the graph.
 * `size` (`uint_8`) is the number of edges.
 * `edges` is a stream of size bytes, each of them considered as a couple 
 * of 4bits integer. This structure is defined as a bitfield edge_t of above
 */
int ReadData(FILE* infile, uint32_t *Id, int *N, int *G, int *Deg ) {
    uint8_t tmp[2];
    edge_t *EL;
    int i;
    
    // Read the Id
    if( fread(Id, sizeof(uint32_t), 1, infile) == 0 ) //end of file?
        return 0;
    
    // Read the graph order and number of elements
    if( fread(tmp, sizeof(uint8_t), 2, infile) < 2 ) //WTF
        abort();
    
    *N = (int)tmp[0];
    
    // Allocate memory for reading the edge list
    EL = (edge_t*)malloc( ((size_t)tmp[1]) * sizeof(edge_t) );

    // Read it!
    if( fread(EL, sizeof(edge_t), (size_t)tmp[1], infile) < (size_t)tmp[1] ) 
        abort();
    
    // OK, now put it as I like it into memory
    for( i = 0 ; i < *N ; i++ ){
        Deg[i] = 0;
    }
    
    // Put the neighbours
    for( i = 0 ; i < tmp[1] ; i++ ){
        G[ (*N-1) * EL[i].u + Deg[EL[i].u] ] = EL[i].v;
        G[ (*N-1) * EL[i].v + Deg[EL[i].v] ] = EL[i].u;
        Deg[EL[i].u] += 1;
        Deg[EL[i].v] += 1;
    }
    
    //Free memory
    free(EL);
    return 1;
}

/*
 * Write data to a file formated as explained in FileFormats.md:
 * Again, the format tries to be as simple and compact as possible. It contains 
 * matrices Q and b needed to compute the fixation probabilities as Qx=b.
 * These matrices are stored in a sparse format:
 *
 * [ Id | N | num_Q | Q | num_b | b ]
 *
 * where
 *  - Id (uint32_t) is the unique Id of the graph. 
 *  - N (uint8_t) is the number of nodes in the graph.
 *  - num_Q (uint_16) is the number nontrivial transitions.
 *  - Q this is a concatenation num_Q groups of 
 *
 *        [ A | B | Rn | Rd | In | Id ]
 *
 *    where A and B are the states (represented by uint16_t) and Rn, Rd, In and 
 *    Id are int16_t to form the corresponding value Rn/Rd*r+In/Ir.
 *  - num_b (uint_16) is the number of nontrivial entrances in b.
 *  - b is a concatenation similar to Q
 *       [ A | Rn | Rb | In | Id ]
 *    where A is a state (represented by a uint16_t) and Rn, Rd, In and Id 
 *    are int16_t to form the corresponding value Rn/Rd*r+In/Ir.

 */
void WriteData(FILE* outfile, uint32_t IdG, uint8_t N, int64_t* Rd, int64_t* Rn, int64_t* Id, int64_t* In) {
    int i,j,x;
    
    //Write graphs Id
    if(fwrite(&IdG, sizeof(uint32_t), 1, outfile) == 0){
        printf("Error writing to file\n");
        abort();
    }
    //Write graph order
    if(fwrite(&N, sizeof(uint8_t), 1, outfile) == 0){
        printf("Error writing to file\n");
        abort();
    }
    
    Q_t* Q = (Q_t*)malloc( (1L<<N)*((size_t)(N+1)) * sizeof(Q_t) );
    b_t* b = (b_t*)Q;
    
    uint16_t num=0;
    
    //Saving the matrix in memory
    for( i = 1 ; i < (1L<<N)-1 ; i++) {
        for(j = 0 ; j < N ; j++ ){
            x = (N+2) * i + j;
            if(!IsNull(Rn[x], Rd[x], In[x], Id[x])){
                //Save this transition
                Q[num].A  = (uint16_t)i;
                Q[num].B  = ( (uint16_t)i ^ (uint16_t)(1<<j) );
                Q[num].Rn = (int16_t)(Rn[x]);
                Q[num].Rd = (int16_t)(Rd[x]);
                Q[num].In = (int16_t)(In[x]);
                Q[num].Id = (int16_t)(Id[x]);
                num+=1;
            }
        }
        x = (N+2) * i + N;
        if(!IsNull(Rn[x], Rd[x], In[x], Id[x])){
            //Save this transition
            Q[num].A  = (uint16_t)i;
            Q[num].B  = (uint16_t)i;
            Q[num].Rn = (int16_t)(Rn[x]);
            Q[num].Rd = (int16_t)(Rd[x]);
            Q[num].In = (int16_t)(In[x]);
            Q[num].Id = (int16_t)(Id[x]);
            num+=1;
        }
    }
    //write it to disk
    if(fwrite(&num, sizeof(uint16_t), 1, outfile) == 0){
        printf("Error writing to file\n");
        abort();
    }
    if(fwrite(Q, sizeof(Q_t), num, outfile) < num){
        printf("Error writing to file\n");
        abort();
    }

    num = 0;
    for( i = 1 ; i < (1L<<N)-1 ; i++) {
        x = (N+2) * i + N + 1;
        if(!IsNull(Rn[x], Rd[x], In[x], Id[x])){
            //Save this transition
            b[num].A  = (uint16_t)i;
            b[num].Rn = (int16_t)(Rn[x]);
            b[num].Rd = (int16_t)(Rd[x]);
            b[num].In = (int16_t)(In[x]);
            b[num].Id = (int16_t)(Id[x]);
            num+=1;
        }
    }
    //write it to disk
    if(fwrite(&num, sizeof(uint16_t), 1, outfile) == 0){
        printf("Error writing to file\n");
        abort();
    }
    if(fwrite(b, sizeof(b_t), num, outfile) < num){
        printf("Error writing to file\n");
        abort();
    }
    
    free(Q);
}




