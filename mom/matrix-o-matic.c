#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>
#include <mpi.h>

#include "types.h"
#include "tools.h"
#include "iotools.h"
#include "bpsolver.h"

const int NSOL = 40;
const int64_t DSTEP = 4;


/* 
 * We have the (linear in r) matrix
 *
 *      Rn/Rd * r + In/Id
 *
 * Where the columns are (i is the row)
 *      First N columns -> the change of state from i to i with the column-th bit changed
 *      prelast         -> The diagonal of the Matrix Q
 *      last            -> The vector b
 *
 *
 * The matrix G is the graph: G[ (N-1) * u + i ] is the i-th neighbour of u 
 * It should be N * (N-1)
 *
 *
 * Deg is the vector of degrees
 *
 * ----------------------------------------------------------------
 * There is room for memory optimizations using Deg to reduce the amount of 
 * used memory, but probably it will make things worse for a very little gain
 */
void system_matrices( int N, int *G, int *Deg, int64_t *Rd, int64_t *Rn, int64_t *Id, int64_t *In ) {
    
    uint32_t conf;      // Origin configuration
    uint32_t dest;      // Destination configuration
    uint32_t NMuta;     // Number of mutants
    int u;              // Selected to reproduction
    int v;              // Selected to be replaced
    uint32_t uMutant;   // Is uMutant?
    int x;              // general use temporal variable. To be used just in 
                        // few lines after wards
    int i;              // General iterator
    int64_t gcd,lcm;    // For the operations with fractions a/b
    
    //Initializations
    const int NStates      = (1<<N);
    const int NTrans       = N + 2;
    const uint32_t MAXCONF = ((1<<N)-1);
        
    for( i = 0 ; i < NStates * NTrans ; i++ ){
        // To-Do: use memset and check if there is some speed improvement.
        // Even better... use malloc here instead of in the main loop. 
        Rn[i] = In[i] = 0;
        Rd[i] = Id[i] = 1;
    }
    
    // Start computations
    for( conf = 1 ; conf < MAXCONF ; conf++ ){
        // Compute the number of mutants in the configuration conf
        NMuta = __builtin_popcount(conf);
        
        // Q is central part of I-P, so we need to add elements to the diagonal
        // but we do the thing normalizig each row of the ecuations...
        // as all of them share the same denominator. So, multiplyng with this
        // denominator we get polynomials... everything much more quick
        x = NTrans * conf + N;
        Rn[x] = NMuta;
        In[x] = N - NMuta;
        //This is not needed Rd[ x ] = Id[ x ] = 1; //They are already ones
        
        // Iterate over all the nodes
        for( u = 0 ; u < N ; u++ ){
            
            uMutant = conf & (1<<u);
            // u is selected for reproduction
            
            for( i = 0 ; i < Deg[u] ; ++i ){
                v = G[(N-1)*u+i];
                // v will be replaced
                
                if(uMutant){ // u is mutant
                    // take conf and add to it a 1 in the position v
                    dest = conf | (1<<v);
                    
                    if(dest == MAXCONF){ 
                        // dest is the state where all are mutants
                        x     = NTrans * conf + N + 1;
                        
                        Rn[x] = Rn[x] * Deg[u] + Rd[x];
                        Rd[x] = Rd[x] * Deg[u];
                        // Some simplifications
                        if(Rn[x]==0){Rd[x]=1;}else{gcd=GCD(Rn[x],Rd[x]);Rn[x]/=gcd;Rd[x]/=gcd;}
                    } else {
                        // dest is not the state without mutants since we are adding a bit
                        // where we save the new data depends on how dest is 
                        // with respect to conf
                        x     = NTrans * conf + (dest==conf?N:v);
                        Rn[x] = Rn[x] * Deg[u] - Rd[x];
                        Rd[x] = Rd[x] * Deg[u];
                        // Some simplifications
                        if(Rn[x]==0){Rd[x]=1;}else{gcd=GCD(Rn[x],Rd[x]);Rn[x]/=gcd;Rd[x]/=gcd;}
                    }
                } else { // u is a resident
                    //Take conf and put a 0 at position v
                    dest = conf & ( ~(1<<v) );
                    
                    // Removing one bit... it is impossible to get to MAXCONF
                    if(dest > 0){
                        // dest is not the state without mutants
                        // where we save the new data depends on how dest is 
                        // with respect to conf
                        x     = NTrans * conf + (dest==conf?N:v);
                        In[x] = In[x] * Deg[u] - Id[x];
                        Id[x] = Id[x] * Deg[u];
                        // Some simplifications
                        if(In[x]==0){Id[x]=1;}else{gcd=GCD(In[x],Id[x]);In[x]/=gcd;Id[x]/=gcd;}
                    }
                }
            }
        }
    }
}


/*
 * Compute solutions
 *
 *
 */
void ComputeAndWriteSolutions(uint32_t IdG, uint8_t N, int* G, int *Deg, int64_t* Rd, int64_t* Rn, int64_t* Id, int64_t* In, FILE* outfile) {
    
    int nr;
    unsigned int TAM = ((1UL<<N)-2);
    int i,j,k;      // counters
    int x,y,z;      // array positions
    real_t r;
    real_t *Q = (real_t*)malloc( TAM * TAM * sizeof(real_t) );
    real_t *b = (real_t*)malloc( TAM * sizeof(real_t) );
    real_t *Qsum = (real_t*)malloc( TAM * sizeof(real_t) );
    real_t *X = (real_t*)malloc( TAM * sizeof(real_t) );
    
    
    
    // the fitness is i/DSTEP
    r = (double)IdG;
    if( fwrite(&r, sizeof(real_t), 1, outfile) == 0 ){
        printf("Write error!\n");
        abort();
    }
    //fprintf(outfile, "%u", IdG);
    for( nr = 1 ; nr < NSOL+1 ; nr++ ){
        r = ((real_t)nr)/DSTEP;
        //Clean the matrices
        for( i = 0 ; i < TAM * TAM ; i++ )
            Q[i] = 0.0;
        for( i = 0 ; i < TAM ; i++ )
            b[i] = X[i] = 0.0;
        
        // Construct the matrices. Let us start with Q
        for( i = 1 ; i < (1UL<<N)-1 ; i++) {
            for( j = 0 ; j < N ; j++ ){
                x = (N+2) * i + j;
                if( !IsNull(Rn[x], Rd[x], In[x], Id[x]) ){
                    // R*r+I
                    y = TAM*(i-1) + (i ^ (1U<<j)) - 1;
                    Q[y]  = ((real_t)(Rn[x])) / ((real_t)(Rd[x]));
                    Q[y] *= r;
                    Q[y] += ((real_t)(In[x])) / ((real_t)(Id[x]));
                }
            }
            x = (N+2) * i + N;
            if( !IsNull(Rn[x], Rd[x], In[x], Id[x]) ){
                // R*r+I
                y = TAM*(i-1) + i - 1;
                Q[y]  = ((real_t)(Rn[x])) / ((real_t)(Rd[x]));
                Q[y] *= r;
                Q[y] += ((real_t)(In[x])) / ((real_t)(Id[x]));
            }
        }
        
        // Now b
        for( i = 0 ; i < TAM ; i++) {
            x = (N+2) * (i+1) + N + 1;
            if( !IsNull(Rn[x], Rd[x], In[x], Id[x]) ){
                // R*r+I
                b[i]  = ((real_t)(Rn[x])) / ((real_t)(Rd[x]));
                b[i] *= r;
                b[i] += ((real_t)(In[x])) / ((real_t)(Id[x]));
            }
        }
        
        // Compute the sum of the row of Q. 
        // We will use the graphs structure to compute it since it is
        // sum over the neigbours of the mutant of 1/(deg[neighbour])
        for( i = 0 ; i < TAM ; i++)
            Qsum[i] = b[i];
        
        for( i = 0 ; i < N ; i++ ){
            x = (1<<i) - 1;
            for( j = 0 ; j < Deg[i] ; j++ )
                Qsum[x] += (1.0L)/(real_t)(Deg[ G[(N-1)*i+j] ]);
        }
        
        
        bpsolver(TAM, Q, b, Qsum, X);
        
        real_t PF = 0.0;
        
        for( i = 0 ; i < N ; i++)
            PF += X[(1<<i)-1];
        PF /= (real_t)N;
        
        //fprintf(outfile, "\t%.*e", 60, PF);
        if( fwrite(&PF, sizeof(real_t), 1, outfile) == 0 ){
            printf("Write error!\n");
            abort();
        }
        
    }
    //fprintf(outfile, "\n");
    
    free(Q);
    free(b);
    free(X);
    free(Qsum);
}



/*
 * Entry point
*/
int main(int argc, char *argv[]) {
    
    // Counters
    int my_id, root_process, ierr, num_procs;
    int i;
    int j;
    int x;                  // Convoluted position in a 2d-matrix
    const int NMax = 10;    // Maximal number of nodes we are going to find
    char infilename[1024];  // Input file name
    FILE *infile;           // Input file pointer
    char outfilename[1024]; // Output file name
    FILE *outfile;          // Output file pointer
    int N;                  // Actual number of nodes
    uint32_t IdG;           // Id of the graph
    char* path;
    
    //Get the starting point
    if(argc < 3){
        printf("Usage: %s <path> <i>\n\nwhere \n<path> is the path where the files are stored\n<i> is the starting point of the computing.\n",argv[0]);
    }
    path = argv[1];
    x = atoi(argv[2]);
    
    // Initalize the MPI thing
    MPI_Status status;
    ierr = MPI_Init(&argc, &argv);
    
    ierr = MPI_Comm_rank(MPI_COMM_WORLD, &my_id);
    ierr = MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
    
    x += my_id;
    sprintf(infilename, "%s/EdgeLists%i.bin", path, x);
    sprintf(outfilename, "%s/fp_10_%i", path, x);
    
    infile = fopen(infilename, "rb");
    if(!infile){
        printf("Error! Unable to open '%s' for reading\n\n", infilename);
        return 1;
    }
    
    outfile = fopen(outfilename, "wb");
    if(!outfile){
        printf("Error! Unable to open '%s' for writing\n\n", outfilename);
        fclose(infile);
        return 1;
    }
    
    // OK we're ready for rock!
    
    // Allocate memory for the maximal case
    int *G      = (    int*)malloc(      NMax*(NMax-1) * sizeof(int)     );
    int *Deg    = (    int*)malloc(               NMax * sizeof(int)     );
    int64_t *Rn = (int64_t*)malloc( (1<<NMax)*(NMax+2) * sizeof(int64_t) );
    int64_t *Rd = (int64_t*)malloc( (1<<NMax)*(NMax+2) * sizeof(int64_t) );
    int64_t *In = (int64_t*)malloc( (1<<NMax)*(NMax+2) * sizeof(int64_t) );
    int64_t *Id = (int64_t*)malloc( (1<<NMax)*(NMax+2) * sizeof(int64_t) );
    
    time_t start = time(NULL);
    time_t end;
    
    printf("It is %i working in %s and writing to $s.\n", my_id, infilename, outfilename);
    
    while( ReadData( infile, &IdG, &N, G, Deg) ) {
        system_matrices(N, G, Deg, Rd, Rn, Id, In);
        ComputeAndWriteSolutions(IdG, N, G, Deg, Rd, Rn, Id, In, outfile);
    }
    
    // Close files
    fclose(infile);
    fclose(outfile);
    
    // Free memory
    free(  G);
    free(Deg);
    free( Rn);
    free( Rd);
    free( In);
    free( Id);
    
    // bye bye
    ierr = MPI_Finalize();
}


