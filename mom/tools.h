
#ifndef _TOOLS_H
#define _TOOLS_H

#include "types.h"

/*
 * Computes the greatest common divisor of 2 integers
*/
int64_t GCD(int64_t a, int64_t b);

/*
 * Checks if the polinomial a/b r + c/d is null
*/
int IsNull(int64_t a, int64_t b, int64_t c, int64_t d);

/*
 * Computes a/b * c/d and saves it in rn/rd
*/
void MulTo(int64_t* rn, int64_t* rd, int64_t a, int64_t b, int64_t c, int64_t d);
//rational_t Prod(rational_t a, rational_t b);

/*
 * Computes a/b + c/d and saves it in rn/rd
*/
void AddTo(int64_t* rn, int64_t* rd, int64_t a, int64_t b, int64_t c, int64_t d);
//rational_t Add(rational_t a, rational_t b);

/*
 * Prints a fraction, nicely
*/
void PrintFraction(int64_t c, int64_t d);
    



#endif
