

#ifndef _TYPES_H
#define _TYPES_H


// bitfield for the infile
typedef struct {
    uint8_t u : 4;
    uint8_t v : 4;
} edge_t;

//Structures for the outfile
typedef struct {
    uint16_t A;
    uint16_t B;
    int16_t Rn;
    int16_t Rd;
    int16_t In;
    int16_t Id;
} Q_t;

typedef struct {
    uint16_t A;
    int16_t Rn;
    int16_t Rd;
    int16_t In;
    int16_t Id;
} b_t;


#define DOUBLE

#ifdef FLOAT
#define real_t float
#endif

#ifdef DOUBLE
#define real_t double
#endif

#ifdef LONG_DOUBLE
#define real_t long double
#endif

#ifdef FLOAT128
#include <quadmath.h>
#define real_t __float128
#endif


#endif
