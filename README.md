# Matrix-o-Matic #

This little software was used to compute the database of fixation probabilities of the paper [doi 10.1007/978-3-319-56154-7_20](https://doi.org/10.1007/978-3-319-56154-7_20). The exact version of the software used in that paper is tagged with "PaperDB".

##### Changelog

[20180503] A bug on `bpsolver.c` which produced wrong results on
some circumstances has been corrected. This bug did not affect the computation of fixation probabilities, though.